﻿using static XNavigation;

internal static partial class XNavigationSpacePrivate
{
    public struct Triangle {
        public TriangleRef edgeABNeighbourTriangleRef;
        public TriangleRef edgeACNeighbourTriangleRef;
        public TriangleRef edgeBCNeighbourTriangleRef;

        public void setNeighbourTriangleRefByIndex(int inIndex, TriangleRef inNewValue) {
            //TODO: Swtch access: make index as internal base and 
            // Or event make access by index only for building triangle?
            switch(inIndex) {
                case 0: edgeABNeighbourTriangleRef = inNewValue; break;
                case 1: edgeACNeighbourTriangleRef = inNewValue; break;
                case 2: edgeBCNeighbourTriangleRef = inNewValue; break;
                default: XUtils.check(false); break;
            }
        }

        public VertexRef vertexARef;
        public VertexRef vertexBRef;
        public VertexRef vertexCRef;

        public PlaneRef planeRef;
    }

    public struct NeighboursBuilding
    {
        public NeighboursBuilding(int inVerticesNum) {
            _vertexInfos = new FastArray<VertexInfo>(inVerticesNum, false);
            for (int theIndex = 0; theIndex < inVerticesNum; ++theIndex) {
                _vertexInfos[theIndex] = new VertexInfo(3);
            }
        }

        public TriangleRef checkEdge(VertexRef inVertexARef, VertexRef inVertexBRef, TriangleRef inEdgeTriangle) {
            XUtils.check(inVertexARef.index < inVertexBRef.index);
            return _vertexInfos[inVertexARef.index].checkNeighbourVertex(inVertexBRef, inEdgeTriangle);
        }

        private struct VertexInfo {
            public VertexInfo(int inInitialSize) {
                neighbourVerticesToCheck = new FastArray<NeighbourVertexToCheck>(inInitialSize);
            }

            public TriangleRef checkNeighbourVertex(VertexRef inNeighbourVertexRef, TriangleRef inTriangleRef) {
                clojureFor_comparator = inNeighbourVertexRef;
                Optional<int> theExisingNeighbourIndex = neighbourVerticesToCheck.findIndex(comparator);

                TriangleRef theResult = TriangleRef.invalid;
                if (theExisingNeighbourIndex.isSet()) {
                    theResult = neighbourVerticesToCheck[theExisingNeighbourIndex.value].triangleRef;
                    neighbourVerticesToCheck.removeElementAt(theExisingNeighbourIndex.value, true);
                } else {
                    neighbourVerticesToCheck.add(new NeighbourVertexToCheck(inNeighbourVertexRef, inTriangleRef));
                }
                return theResult;
            }

            private static VertexRef clojureFor_comparator;
            private static System.Func<NeighbourVertexToCheck, bool> comparator =
                (NeighbourVertexToCheck inExistingNeighbourVertex) =>
            {
                return inExistingNeighbourVertex.vertexRef.Equals(clojureFor_comparator);
            };

            private struct NeighbourVertexToCheck {
                public NeighbourVertexToCheck(VertexRef inVertexRef, TriangleRef inTriangleRef) {
                    vertexRef = inVertexRef; triangleRef = inTriangleRef;
                }

                public VertexRef vertexRef;
                public TriangleRef triangleRef;
            }

            private FastArray<NeighbourVertexToCheck> neighbourVerticesToCheck; //TODO: Optimize using of memory for this array
        }

        private FastArray<VertexInfo> _vertexInfos;
    }
}
