﻿using UnityEngine;

using static XNavigation;
using XMathTypes;

internal static partial class XNavigationSpacePrivate
{
    public static void buildNavigationSpace(NavigationSpace inSpace, Vector3[] inVertices, int[] inTriangles) {
        XUtils.check(XUtils.isValid(inVertices) && XUtils.isValid(inTriangles));
        XUtils.check(0 == inTriangles.Length % 3);

        reindex(inTriangles);

        int theTrianglesNum = inTriangles.Length / 3;

        inSpace.verticies = new FastArray<Vector3>(inVertices);
        inSpace.triangles = new FastArray<Triangle>(theTrianglesNum);
        inSpace.planes = new FastArray<OrientedPlane>();

        NeighboursBuilding theNeighboursBuilding = new NeighboursBuilding(inVertices.Length);

        for (int theTriangleIndex = 0, theFirstTriangleVertexIndex = 0; theTriangleIndex < theTrianglesNum;
            ++theTriangleIndex, theFirstTriangleVertexIndex += 3)
        {
            VertexRef theVertexARef = inTriangles[theFirstTriangleVertexIndex + 0];
            VertexRef theVertexBRef = inTriangles[theFirstTriangleVertexIndex + 1];
            VertexRef theVertexCRef = inTriangles[theFirstTriangleVertexIndex + 2];
            sort(ref theVertexARef.index, ref theVertexBRef.index, ref theVertexCRef.index);

            ref Triangle theNewTriangle = ref inSpace.triangles.getRef(theTriangleIndex);
            theNewTriangle.vertexARef = theVertexARef;
            theNewTriangle.vertexBRef = theVertexBRef;
            theNewTriangle.vertexCRef = theVertexCRef;

            TriangleRef theNewTriangleRef = new TriangleRef(inSpace.triangles.getSize());

            processEdgeNeighbour(ref theNeighboursBuilding, inSpace, theVertexARef, theVertexBRef, theNewTriangleRef, 0);
            processEdgeNeighbour(ref theNeighboursBuilding, inSpace, theVertexARef, theVertexCRef, theNewTriangleRef, 1);
            processEdgeNeighbour(ref theNeighboursBuilding, inSpace, theVertexBRef, theVertexCRef, theNewTriangleRef, 2);

            setTrianglePlane(inSpace, ref theNewTriangle);
        }
    }

    private static void reindex(int[] inIndices) {
        int theNum = inIndices.Length;
        int[] theReindexMapping = new int[theNum];
        XUtils.setArray(theReindexMapping, -1);
        int theNextIndexToAssign = 0;
        for (int theIndex = 0; theIndex < theNum; ++theIndex) {
            int theOldIndex = inIndices[theIndex];
            int theNewIndex = theReindexMapping[theOldIndex];
            if (-1 == theNewIndex) {
                theNewIndex = theNextIndexToAssign++;
                theReindexMapping[theOldIndex] = theNewIndex;
            }
            inIndices[theIndex] = theNewIndex;
        }
    }

    private static void sort(ref int inVertexAIndex, ref int inVertexBIndex, ref int inVertexCIndex) {
        if (inVertexAIndex > inVertexBIndex) XUtils.swap(ref inVertexAIndex, ref inVertexBIndex);
        if (inVertexBIndex > inVertexCIndex) XUtils.swap(ref inVertexBIndex, ref inVertexCIndex);
        if (inVertexAIndex > inVertexBIndex) XUtils.swap(ref inVertexAIndex, ref inVertexBIndex);
    }

    private static void processEdgeNeighbour(ref NeighboursBuilding inNeighboursBuilding, NavigationSpace inSpace,
        VertexRef inVertexARef, VertexRef inVertexBRef, TriangleRef inTriangleRef, int inEdgeIndex)
    {
        TriangleRef theNeighbourTrianlgeRef = inNeighboursBuilding.checkEdge(inVertexARef, inVertexBRef, inTriangleRef);
        if (theNeighbourTrianlgeRef.isValid) {
            ref Triangle theTriangle = ref inSpace.triangles.getRef(inTriangleRef.index);
            ref Triangle theNeighbourTrianlge = ref inSpace.triangles.getRef(theNeighbourTrianlgeRef.index);

            theTriangle.setNeighbourTriangleRefByIndex(inEdgeIndex, theNeighbourTrianlgeRef);
            theNeighbourTrianlge.setNeighbourTriangleRefByIndex(inEdgeIndex, inTriangleRef);
        }
    }

    private static void setTrianglePlane(NavigationSpace inSpace, ref Triangle inTriangle) {
        //TODO: Implement

        //planes
        //inTriangle
    }

    //OrientedPlane getTrianglePlane(Triangle inTriangle) {
    //
    //}
}
