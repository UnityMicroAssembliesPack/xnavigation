﻿
public static partial class XNavigation
{
    private static int kInvalidRefIndex = -1;

    public struct VertexRef : System.IEquatable<VertexRef> {
        public VertexRef(int inIndex) { index = inIndex; }
        public bool Equals(VertexRef inOther) { return (inOther.index == index); }

        public bool isValid { get { return index != kInvalidRefIndex; } }
        public static VertexRef invalid { get { return new VertexRef(kInvalidRefIndex); } }

        public static implicit operator VertexRef(int inIndex) => new VertexRef(inIndex);

        internal int index;
    }

    public struct TriangleRef : System.IEquatable<TriangleRef> {
        public TriangleRef(int inIndex) { index = inIndex; }
        public bool Equals(TriangleRef inOther) { return (inOther.index == index); }

        public bool isValid { get { return index != kInvalidRefIndex; } }
        public static TriangleRef invalid { get { return new TriangleRef(kInvalidRefIndex); } }

        internal int index;
    }

    public struct PlaneRef : System.IEquatable<PlaneRef>
    {
        public PlaneRef(int inIndex) { index = inIndex; }
        public bool Equals(PlaneRef inOther) { return (inOther.index == index); }

        public bool isValid { get { return index != kInvalidRefIndex; } }
        public static PlaneRef invalid { get { return new PlaneRef(kInvalidRefIndex); } }

        internal int index;
    }
}
