﻿using UnityEngine;
using static XNavigationSpacePrivate;
using XMathTypes;

public static partial class XNavigation
{
    public class NavigationSpace {
        public NavigationSpace(Vector3[] inVertices, int[] inTriangles) {
            buildNavigationSpace(this, inVertices, inTriangles);
        }

        internal FastArray<Vector3> verticies = null;
        internal FastArray<Triangle> triangles = null;
        internal FastArray<OrientedPlane> planes = null;
    }
}
